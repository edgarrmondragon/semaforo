import csv
import xlwt
from string import ascii_lowercase, ascii_uppercase

# Cell format
EASYXF_FORMAT = """font: bold 0, color {font_color:s}, name Calibri;
align: wrap on, vert centre, horiz center;
border: ;
pattern: pattern solid, fore_colour {style:s};
"""

MOCKDATA = [
    ['10bcDE', '5de', '5de', '1', '1'],
    ['3.00B', '2.53', '3.21aBd', '3.07B', '3.51ABCD']
]

DEFAULT_STYLES = {
    'FONT_COLOR': {
        'win1': 'white',
        'win2': 'white',
        'lose1': 'white',
        'lose2': 'white',
        'neutral': 'black',
        'ref': 'black'
    },
    'BG_COLOR': {
        'win1': (0, 176, 80),
        'win2': (146, 208, 80),
        'lose1': (255, 0, 0),
        'lose2': (255, 102, 0),
        'neutral': (255, 192, 0),
        'ref': (155, 194, 230)
    }
}

styles = [
    'win1',
    'win2',
    'lose1',
    'lose2',
    'neutral',
    'ref'
]

# Palette colours
for i, style in enumerate(styles):
    xlwt.add_palette_colour(style, 0x21 + i)

# Create Workbook object
workbook = xlwt.Workbook()

EASYXF = dict()
for i, style in enumerate(styles):
    # easyxf
    EASYXF[style] = xlwt.easyxf(
        EASYXF_FORMAT.format(
            font_color=DEFAULT_STYLES['FONT_COLOR'][style],
            style=style
        )
    )
    
    # Set colors
    workbook.set_colour_RGB(
        0x21 + i,
        *(DEFAULT_STYLES['BG_COLOR'][style])
    )

for reference in range(5):
    ref_lower = ascii_lowercase[reference]
    ref_upper = ascii_uppercase[reference]
    sheet = workbook.add_sheet('{:0>02d}'.format(reference + 1))
    for i, row in enumerate(MOCKDATA):

        # Catch losers
        big_losers = []
        small_losers = []
        for k in range(26):
            if ascii_uppercase[k] in row[reference]:
                big_losers.append(k)
            elif ascii_lowercase[k] in row[reference]:
                small_losers.append(k)
        
        # Catch winners
        for j, value in enumerate(row):
            if j == reference:
                sheet.write(i, j, value, EASYXF['ref'])
                continue
            if ref_upper in value:
                sheet.write(i, j, value, EASYXF['win1'])
            elif ref_lower in value:
                sheet.write(i, j, value, EASYXF['win2'])
            elif j in big_losers:
                sheet.write(i, j, value, EASYXF['lose1'])
            elif j in small_losers:
                sheet.write(i, j, value, EASYXF['lose2'])
            else:
                sheet.write(i, j, value, EASYXF['neutral'])
            pass

workbook.save("test.xls")
