from distutils.core import setup

setup(
    name='Semaforo',
    version='0.1dev',
    pymodules=['semaforo'],
    license='MIT'
)
